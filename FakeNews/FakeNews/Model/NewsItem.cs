﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace FakeNews.Model
{
     public class NewsItem
    {
        public int Id { get; set; }
        public string Category { get; set; }
        public string HeadLine { get; set; }
        public string Subhead { get; set; }
        public string Dateline { get; set; }
        public string Image { get; set; }
    }
    public class NewsManager
    {
        public static void GetNews(string category, ObservableCollection<NewsItem> newsItems)
        {
            var allItems = getNewsItems();
            var filteredNewsItem = allItems.Where(p => p.Category == category).ToList();
            newsItems.Clear();
            filteredNewsItem.ForEach(p => newsItems.Add(p));
        }
        private static List<NewsItem> getNewsItems()
        {
            var items = new List<NewsItem>();

            items.Add(new NewsItem() { Id = 1, Category = "Financial",
                  HeadLine = "Lorem Ipsum",
                   Subhead = "doro sit amet", Dateline = "Nunc tristique nec",
            Image = "Assets/Images02/Financial1.png"});
            items.Add(new NewsItem() { Id = 2, Category = "Financial",
                HeadLine = "Etiam ac felis viverra", Subhead = " vulputate nisl ac, aliquet nisi",
                Dateline = "tortor porttitor,eu fermentum ante congue", Image = "Assets/Images02/Financial2.png" });
            items.Add( new NewsItem(){ Id = 3 ,Category = "Financial",
                 HeadLine = "Interger sed tupis erat",
                 Dateline = "In viverra metus facilisis sed", Image = "Assets/Images02/Financial3.png"});
            items.Add(new NewsItem() { Id = 4,Category ="Financial",
                 HeadLine = "proin sem neque", Subhead ="aliquet quis ipsum tincidunt",
                 Dateline = "interger eleifend", Image = "Assets/Images02/Financial4.png"});
            items.Add(new NewsItem() { Id = 5, Category = "Financial",
                 HeadLine ="Mauris bibendum non leo vitae tempor",
                 Subhead = "in nisl tortor,eleifend sed ipsum eget",
                 Dateline ="Curabitur dictum augue vitae elementum ultrices", Image = "Assets/Images02/Finicial5.png"});
             items.Add(new NewsItem() { Id = 6, Category ="Food",
                 HeadLine = "Lorem ipsum", Subhead ="dolor sit amet",
                 Dateline = "Nunc tristique nec", Image = "Assets/Images02/Food1.png"});
             items.Add(new NewsItem() { Id = 7, Category ="Food",
                 HeadLine = "vulputate nisl ac,aliquet nisi",
                 Dateline = "tortor porttitor,eu fementum ante congue", Image = "Assets/Images02/Food2.png"});
             items.Add(new NewsItem() { Id = 8, Category ="Food",
                 HeadLine = "interger sed turpis erat",
                 Subhead ="sed quis hendrerit lorem,quis interdum dolor",
                 Dateline = "Inviverra metus facilissis sed", Image = "Assets/Images02/Food3.png"});
             items.Add(new NewsItem() { Id = 9, Category ="Food",
                 HeadLine = "proin sem neque",
                 Subhead ="aliquet quis ipsum tinclidunt",
                 Dateline = "interger eleifend", Image = "Assets/Images02/Food4.png"});
             items.Add(new NewsItem() { Id = 10, Category ="Food",
                 HeadLine = "mauris bibendum non leo vitae tempor",
                 Subhead ="in nisl tortor,eleifend sed ipsum eget",
                 Dateline = "curabitur dictum augue vitae elementum ultrices", Image = "Assets/Images02/Food5.png"});

            return items;

            
        }
    }
}
